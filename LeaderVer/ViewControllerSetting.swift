//
//  ViewControllerSetting.swift
//  LeaderVer
//
//  Created by Evelyn on 9/3/2016.
//  Copyright © 2016 Hong Kong Polytechnic University (Electronic and Information Engineering). All rights reserved.
//

import UIKit

class ViewControllerSetting: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIScrollViewDelegate{
    @IBOutlet weak var churchPicker: UIPickerView!
    @IBOutlet weak var displayOptionPicker: UIPickerView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    var ServerListHost = "http://54.213.147.65:8080"
    var ServerListDir = "/churches.json"
    var displayOptionPickerDataSource = [["Wood", "White",  "Light Yellow","Gray",  "Light Green"],["small", "normal", "big"]]
    
    
    var DBhost = "" //     "http://61.93.64.182:3000"
    var mqttHost = ""// "iot.eclipse.org"
    var mqttTopic =  ""//"Missal_FYP"
    var selectedChurch:NSDictionary = NSDictionary()
    var selectedBgColor = UIColor(patternImage: UIImage(named: "wood2.jpg")!)
    
    var churchInfoList:NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        actInd.startAnimating()
        self.scrollView.delegate = self
        getHttpContent()
        self.churchPicker.dataSource = self;
        self.churchPicker.delegate = self;
        self.churchPicker.reloadAllComponents()
        self.displayOptionPicker.dataSource = self;
        self.displayOptionPicker.delegate = self;
        self.displayOptionPicker.reloadAllComponents()
        self.view.backgroundColor = self.selectedBgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "saveSettingAndGoToMainUI"){
            let destinationNavigationController = segue.destinationViewController as! UINavigationController
            let targetController = destinationNavigationController.topViewController  as! ViewControllerMain
            //            let targetController = segue.destinationViewController as! ViewControllerMain
            //            targetController.DBhost = self.DBhost;
            //            targetController.mqttHost = self.mqttHost;
            //            targetController.mqttTopic = self.mqttTopic;
            targetController.selectedChurch = self.selectedChurch;
            targetController.selectedBgColor = self.selectedBgColor;
        }
    }
    
    
    func getHttpContent(){
        let session = NSURLSession.sharedSession()
        let url = NSURL(string: (ServerListHost + ServerListDir))
        let request = NSURLRequest(URL: url!)
        
        
        let dataTask = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            if error != nil {
                // handle error
                print("error")
            } else {
                
                do {
                    let HttpJson = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                    self.churchInfoList = HttpJson["churches"] as! NSArray
                    self.selectedChurch = self.churchInfoList[0] as! NSDictionary
                    self.DBhost = (self.churchInfoList[0]["ip"] as? String)!
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        self.churchPicker.reloadAllComponents()
                        self.actInd.hidden = true
                    })
                    
                    
                } catch let error as NSError {
                    // failure
                    print("Fetch failed: \(error.localizedDescription)")
                }
                
                
                
            }
        })
        
        dataTask.resume()
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if(pickerView.isEqual(churchPicker)){
            return 1
        }
        
        if(pickerView.isEqual(displayOptionPicker)){
            return 1
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // return the appropriate number of components, for instance
        if(pickerView.isEqual(churchPicker)){
            return churchInfoList.count;
        }
        
        if(pickerView.isEqual(displayOptionPicker)){
            return displayOptionPickerDataSource[component].count
        }
        
        return 0
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView.isEqual(churchPicker)){
            //            let str = "\(churchInfoList[row]["name"] as! String)\t\(churchInfoList[row]["address"] as! String)"
            let str = "\(churchInfoList[row]["name"] as! String)"
            return str
        }
        if(pickerView.isEqual(displayOptionPicker)){
            return displayOptionPickerDataSource[component][row]
        }
        return "Incorrect"
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView.isEqual(churchPicker)){
            selectedChurch = churchInfoList[row] as! NSDictionary
            DBhost = (churchInfoList[row]["ip"] as? String)!
        }
        
        if(pickerView.isEqual(displayOptionPicker)){
            if (component==0){// selecting background color
                
                if(row == 0)
                {
                    self.selectedBgColor = UIColor(patternImage: UIImage(named: "wood2.jpg")!)
                }
                else if(row == 1)
                {
                    self.selectedBgColor = UIColor.whiteColor();
                }
                else if(row == 2)
                {
                    self.selectedBgColor = UIColor(red: 1, green: 1, blue: 0.8, alpha: 1)//light yellow
                }
                else if (row == 3)
                {
                    self.selectedBgColor = UIColor(red: 0.902, green: 0.902, blue: 0.902, alpha: 1)//gray
                }else if (row == 4)
                {
                    self.selectedBgColor = UIColor(red: 0.902, green: 1, blue: 0.902, alpha: 1)//light green
                }
                else
                {//default
                    self.selectedBgColor = UIColor.whiteColor();
                }
                
                
                self.view.backgroundColor = self.selectedBgColor
            }
            //            if (component==1){// selecting font size
            //
            //                if(row == 0)//small
            //                {
            //                    self.selectedFontSize = 20
            //                }
            //                else if(row == 1)//normal
            //                {
            //                    self.selectedFontSize = 30
            //                }
            //                else if(row == 2)//big
            //                {
            //                    self.selectedFontSize = 40
            //                }
            //            }
        }
    }
    
}

