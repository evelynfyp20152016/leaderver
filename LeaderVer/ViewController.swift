//
//  ViewController.swift
//  LeaderVer
//
//  Created by Evelyn on 7/1/16.
//  Copyright © 2016 Hong Kong Polytechnic University (Electronic and Information Engineering). All rights reserved.
//

import UIKit
import Moscapsule

class ViewController: UIViewController {
    
    @IBOutlet weak var DisplayingTitle: UILabel!
    @IBOutlet weak var DisplayingContent: UITextView!
    @IBOutlet weak var imgV1: UIImageView!
    @IBOutlet weak var actInd: UIActivityIndicatorView!
    
    
    //    globel variable for MQTT
    var mqttConfig:MQTTConfig?
    var mqttClient:MQTTClient?
    var mqttHost = "" // "iot.eclipse.org"
    var mqttPort:Int32 = 1883
    var mqttTopic = "" // "Missal_FYP"
    let mqttQos:Int32 = 1;
    var receivedPayload:String?
    
    //    globle variable for HTTP and Json
    var TitleDict:NSDictionary = [
        "entrance_antiphon":"Entrance antiphon",
        "collect":"Collect",
        "prayer_over_the_offerings":"Prayer over the offerings",
        "communication_antiphon":"Communication antiphon",
        "prayer_after_communication":"Prayer after communication",
        "a1":"Reading 1 (Year A)",
        "a2":"Reading 2 (Year A)",
        "gospel_a":"Gospel (Year A)",
        "b1":"Reading 1 (Year B)",
        "b2":"Reading 2 (Year B)",
        "gospel_b":"Gospel (Year B)",
        "c1":"Reading 1 (Year C)",
        "c2":"Reading 2 (Year C)",
        "gospel_c":"Gospel (Year C)"
    ]
    let NumOfImageColumn = 3
    var AllContent: NSMutableArray = NSMutableArray()
    var jsonCurDisplayNum : Int = 0
    var DBhost = ""
    var DBdir = "/public.json"
    var selectedChurch:NSDictionary = NSDictionary()
    var selectedBgColor = UIColor(red: 0.902, green: 0.902, blue: 0.902, alpha: 1)//gray
    var TextViewFontSize:CGFloat = 30
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        actInd.startAnimating()
        DisplayingContent.userInteractionEnabled = true
        DisplayingContent.editable = false
        self.DBhost = self.selectedChurch["ip"] as! String
        self.mqttHost = self.selectedChurch["mqtt_ip"] as! String
        self.mqttTopic = self.selectedChurch["mqtt_topic"] as! String
        
        pageChangingFunc()

        getHttpContent()
        MqttConnection()
        self.imgV1.contentMode = UIViewContentMode.ScaleAspectFit
        self.view.backgroundColor = self.selectedBgColor

        DisplayingContent.font = DisplayingContent.font!.fontWithSize(TextViewFontSize)
        _ = NSTimer.scheduledTimerWithTimeInterval(5.0, target: self, selector: "MqttPublish", userInfo: nil, repeats: true)//leader version only
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageChangingFunc(){
        let swipeRight = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: "handleSwipe:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func MqttConnection(){
        if (mqttHost == ""){
            print("mqttHost not found. mqttHost = \(mqttHost)")
        }else if (mqttTopic == ""){
            print("mqttTopic not found. mqttTopic = \(mqttTopic)")
        }else {
            print("mqttHost = \(mqttHost). mqttTopic = \(mqttTopic)")
            let clientID = NSUUID().UUIDString
            mqttConfig = MQTTConfig(clientId: clientID, host: mqttHost, port: mqttPort, keepAlive: 60)
            mqttConfig!.onPublishCallback = { messageId in
                //                NSLog("published (\(messageId))")
            }
            mqttConfig!.onMessageCallback = { mqttMessage in
//                NSLog("MQTT Message received: payload=\(mqttMessage.payloadString!)")
//                let receivedPayload = mqttMessage.payloadString
//                
//                if ( Int(receivedPayload!) != nil ){
//                    let Num = Int(receivedPayload!)!
//                    self.jsonCurDisplayNum = Num
//                    self.showContent()
//                    
//                }else{
//                    print("Mqtt Reveived Msg Format Incorrecct")
//                }
                
            }
            // create new MQTT Connection
            mqttClient = MQTT.newConnection(mqttConfig!)
            
            self.mqttClient!.subscribe(mqttTopic, qos: mqttQos)
        }
        
    }
    
    
    func MqttPublish(){
        // publish and subscribe
        
        mqttClient!.publishString( String(self.jsonCurDisplayNum), topic: mqttTopic, qos: mqttQos, retain: false)
        //        mqttClient!.publishString(MsgToBeSend, topic: mqttTopic, qos: 1, retain: false)
        
    }
    
    @IBAction func MqttSubcrib(sender: AnyObject) {
        mqttClient!.subscribe(mqttTopic, qos: mqttQos)
    }
    @IBAction func MqttDisconnectBtn(sender: AnyObject) {
        // disconnect
        mqttClient!.disconnect()
        
    }
    
    
    
    func getHttpContent(){
        let session = NSURLSession.sharedSession()
        let url = NSURL(string: (DBhost + DBdir))
        let request = NSURLRequest(URL: url!)
        
        let dataTask = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            if error != nil {
                // handle error
                print("error")
            } else {
                do {
                    let HttpJson = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                    self.formatJsonArray(HttpJson as! NSDictionary)
                    self.showContent()
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.actInd.hidden = true
                    })
                } catch let error as NSError {
                    // failure
                    print("Fetch failed: \(error.localizedDescription)")
                }
            }
        })
        dataTask.resume()
    }
    
    func formatJsonArray( HttpJson: NSDictionary){
        
        let mass        = (HttpJson["mass"] as? NSDictionary)!
        let sequence    = (HttpJson["sequence"] as? NSDictionary)!
        let readings    = (HttpJson["readings"] as? NSDictionary)!
        let tempRites   = (HttpJson["rites"] as? NSArray)!//will be convert to "readings" NSDictionary, avaoid looping repeatly
        var prayers:NSDictionary = NSDictionary()
        var announcements:NSDictionary = NSDictionary()
        if ((HttpJson["prayers"]) != nil){
            prayers = (HttpJson["prayers"] as? NSDictionary)!
        }
        if ((HttpJson["announcements"]) != nil){
            announcements = (HttpJson["announcements"] as? NSDictionary)!
        }
        let homilies    = (HttpJson["homilies"] as? NSArray)!
        let tempSongs       = (HttpJson["songs"] as? NSArray)!
        
        var rites:Dictionary<String, NSDictionary> = [:]
        for var objNum = 0; objNum < tempRites.count; ++objNum {
            let temp = tempRites[objNum] as! NSDictionary
            let title = temp["title"] as! NSString
            rites[title as String] = temp
        }
        
        var songs:Dictionary<Int, NSDictionary> = [:]
        for var objNum = 0; objNum < tempSongs.count; ++objNum {
            let temp = tempSongs[objNum] as! NSDictionary
            let id = temp["id"] as! Int
            songs[id] = (tempSongs[objNum] as! NSDictionary)
        }
        
        do {
            let sequencelist = try NSJSONSerialization.JSONObjectWithData(sequence["content"]!.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!, options: [])//convert String to Json
            for var SeqIndex = 0; SeqIndex < sequencelist.count; ++SeqIndex {//reading "content" in sequence
                let model = sequencelist[SeqIndex]["model"] as! NSString
                let title = sequencelist[SeqIndex]["title"] as! NSString
                //                print("[\(SeqIndex)]it is \(model)")
                if model == "rites" {
                    let riteObj = rites[title as String]
                    if(riteObj != nil){
                        let content = riteObj!.objectForKey("content") as! String
                        self.AllContent.addObject(["type" : "text" , "model" : "\(model)" ,  "title" : title,"content" : content])
                    }else{
                        print("not match! \(title) in sequence's Content[\(SeqIndex)] does not match rites record")
                    }
                }else if model == "readings" {
                    
                    let content = readings[title] as! String
                    self.AllContent.addObject(["type" : "text" , "model" : "\(model)" ,  "title" : (TitleDict.objectForKey(title) as! NSString),"content" : content])
                    
                }else if (model == "prayers")||(model == "homilies")||(model == "announcements") {
                    
                    var imageList:NSArray = NSArray()
                    
                    if model == "prayers" {
                        if (prayers.count>0){ imageList = NSArray(array: [prayers])}
                    }else  if model == "homilies" {
                        imageList = homilies
                    }else  if model == "announcements" {
                        if (announcements.count>0){imageList = NSArray(array: [announcements])}
                    }
                    
                    for var objNum = 0; objNum < imageList.count; ++objNum {
                        for var ImageNum = 0; ImageNum < NumOfImageColumn; ++ImageNum {
                            if (imageList[objNum].objectForKey("image\(ImageNum)") != nil){
                                let url = imageList[objNum].objectForKey("image\(ImageNum)")?.objectForKey("image\(ImageNum)")?.objectForKey("url")
//                                print("url=\(url)")
                                if !(url is NSNull){
                                    print("url=\(url)")
                                    if (url as! String == "null"){
                                        
                                    }else{
                                        self.AllContent.addObject(["type" : "image" , "model" : "\(model)" , "title" : "image\(ImageNum)","content" : "\(DBhost)\(url!)"])
                                    }
                                }
                            }
                        }
                    }
                    
                }else if model == "songs" {
                    let songID = mass[title]
                    if (songID == nil){
                        print("mass[\(title)] is with null value")
                    }else{
                        let song = songs[Int(songID! as! NSNumber)]
                        if(song == nil){
                            print("song with id=\(songID) not found")
                        }
                        let songName = song!["name"] as! String
                        var url = song!["url"]
                        if(url != nil){
                            url = song!["url"]!
                            self.AllContent.addObject(["type" : "image" , "model" : "\(model)" , "title" : songName ,"content" : "\(url!)"])
                        }
                    }
                }else{
                    print("[\(SeqIndex)] it is incorrect format : \(model) ")
                    
                }
                
            }
            
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    func showContent(){
        if ((self.jsonCurDisplayNum < (self.AllContent.count)) && (self.jsonCurDisplayNum>=0)){
            
            let p = self.AllContent[self.jsonCurDisplayNum] as! NSDictionary
            self.imgV1.image = nil
            if (p["type"] as! String)=="image"{
                displayText("", content: "")
                displayImageFromUrl((p["content"] as! String), view: imgV1)
            }else if (p["type"] as! String) == "text"{
                displayText((p["title"] as? String)!, content: (p["content"] as? String)!)
            }
            
        }else{
            print("Mqtt Received Msg Correct But out of range")
        }
    }
    
    
//    @IBAction func NextPage(sender: AnyObject) {
//        if (self.jsonCurDisplayNum < (self.AllContent.count-1)){
//            self.jsonCurDisplayNum++
//            MqttPublish()
//            showContent()
//            
//        }
//        
//    }
//    
//    @IBAction func PreviousPage(sender: AnyObject) {
//        if (self.jsonCurDisplayNum>0){
//            self.jsonCurDisplayNum--
//            MqttPublish()
//            showContent()
//        }
//        
//    }
    
    func NextPage() {
        if (self.jsonCurDisplayNum < (self.AllContent.count-1)){
            self.jsonCurDisplayNum++
            MqttPublish()
            showContent()
            
        }
        
    }
    
    func PreviousPage() {
        if (self.jsonCurDisplayNum>0){
            self.jsonCurDisplayNum--
            MqttPublish()
            showContent()
        }
        
    }
    
    func displayText(title: String , content: String){
        dispatch_async(dispatch_get_main_queue()) {
            self.DisplayingTitle.text = title
            self.DisplayingContent.text = content
            //            self.imgV1.image = nil
            self.imgV1.hidden = true;
        }
    }
    
    
    func displayImageFromUrl(url: String, view: UIImageView){
        
        // Create Url from string
        let url = NSURL(string: url)!
        
        // Download task:
        // - sharedSession = global NSURLCache, NSHTTPCookieStorage and NSURLCredentialStorage objects.
        let task = NSURLSession.sharedSession().dataTaskWithURL(url) { (responseData, responseUrl, error) -> Void in
            // if responseData is not null...
            if let data = responseData{
                
                // execute in UI thread
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.imgV1.hidden = false;
                    view.image = UIImage(data: data)
                })
            }else{
                print("fail to load image: \(url)")
            }
        }
        
        // Run task
        task.resume()
        
    }
    
    @IBAction func handleSwipe(sender: UISwipeGestureRecognizer){
        switch(sender.direction){
            
        case UISwipeGestureRecognizerDirection.Right:
//            print("right")
            self.PreviousPage()
        case UISwipeGestureRecognizerDirection.Left:
//            print("left")
            self.NextPage()
//        case UISwipeGestureRecognizerDirection.Up:
//            print("up")
//        case UISwipeGestureRecognizerDirection.Down:
//            print("down")
            
        default:
            print("Do wrong way")
        }
    }
}

