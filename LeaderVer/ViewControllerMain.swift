//
//  ViewControllerMain.swift
//  LeaderVer
//
//  Created by Evelyn on 9/3/2016.
//  Copyright © 2016 Hong Kong Polytechnic University (Electronic and Information Engineering). All rights reserved.
//

import UIKit

class ViewControllerMain: UIViewController {
    
    @IBOutlet weak var churchTelLabel: UILabel!
    @IBOutlet weak var churchMassTimeLabel: UILabel!
    @IBOutlet weak var churchAddressLabel: UILabel!
    @IBOutlet weak var churchNameLabel: UILabel!
    
    @IBOutlet weak var bgImg: UIImageView!
    
    
    var DBhost = "" //"http://61.93.64.182:3000"
    var mqttHost = "" //"iot.eclipse.org"
    var mqttTopic = "" //"Missal_FYP"
    var selectedChurch:NSDictionary = NSDictionary()
    var selectedBgColor = UIColor(red: 0.902, green: 0.902, blue: 0.902, alpha: 1)//gray
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //        print("Main : DBhost = \(DBhost)")
        //        print("Main : mqttHost = \(mqttHost)")
        //        print("Main : mqttTopic = \(mqttTopic)")
        self.view.backgroundColor = self.selectedBgColor
        self.churchNameLabel.text = self.selectedChurch["name"] as? String
        self.churchAddressLabel.text = "\(self.selectedChurch["address"]!)"
        self.churchMassTimeLabel.text = "\(self.selectedChurch["sunday_mass"]!)"
        self.churchTelLabel.text = "\(self.selectedChurch["telephone"]!)"
        
        self.bgImg.image = UIImage(named: "catholic.jpg")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func enterTodayMissalPage(sender: AnyObject) {
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "goToTodaysMissalUI"){
            let targetController = segue.destinationViewController as! ViewController
            //            targetController.DBhost = self.DBhost;
            //            targetController.mqttHost = self.mqttHost;
            //            targetController.mqttTopic = self.mqttTopic;
            targetController.selectedChurch = self.selectedChurch;
            targetController.selectedBgColor = self.selectedBgColor;
        }
        if (segue.identifier == "goToSettingUI"){
            let targetController = segue.destinationViewController as! ViewControllerSetting
            //            targetController.DBhost = self.DBhost;
            //            targetController.mqttHost = self.mqttHost;
            //            targetController.mqttTopic = self.mqttTopic;
            targetController.selectedChurch = self.selectedChurch;
            targetController.selectedBgColor = self.selectedBgColor;
        }
        
    }
    
}
